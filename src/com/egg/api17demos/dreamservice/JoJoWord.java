package com.egg.api17demos.dreamservice;

import android.os.Handler;
import android.service.dreams.DreamService;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher.ViewFactory;

import com.egg.api17demos.R;

/**
 * JOJOの名言集を表示するサービスです。<br/>
 * {@link DreamService}のAPIはActivityに似せて作られています。
 * 
 * @author yoshihide-sogawa
 * @see http://developer.android.com/about/versions/android-4.2.html#Daydream
 * 
 */
public class JoJoWord extends DreamService {

    /** 名言が切り替わる秒数 */
    private long WORD_SWITCH_TIME = 5000L;

    private TextSwitcher mJojoWord;

    private Handler mHandler;

    private Runnable mRunnbale = new Runnable() {

        @Override
        public void run() {

            mJojoWord.setText(getJojoWords());

            mHandler.postDelayed(mRunnbale, WORD_SWITCH_TIME);
        }
    };

    /**
     * {@link #setContentView(int)}のようなレイアウトのセットアップを行います。<br/>
     */
    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();

        // ユーザのタッチで終了するか(trueの場合は自分でハンドリングする)
        setInteractive(false);
        setFullscreen(true);
        setContentView(R.layout.dreamservice_jojo);

        mJojoWord = (TextSwitcher) findViewById(R.id.dream_service_jojo_word);
        mJojoWord.setFactory(new ViewFactory() {
            @Override
            public View makeView() {
                TextView t = new TextView(getBaseContext());
                t.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
                t.setTextSize(36);
                return t;
            }
        });

        final Animation in = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
        final Animation out = AnimationUtils.loadAnimation(this, android.R.anim.fade_out);
        mJojoWord.setInAnimation(in);
        mJojoWord.setOutAnimation(out);
    }

    /**
     * Dreamが始まった時のアニメーションなどを設定します。
     */
    @Override
    public void onDreamingStarted() {
        super.onDreamingStarted();
        mHandler = new Handler();
        mHandler.post(mRunnbale);
    }

    /**
     * {@link #onDreamingStarted()}が停止された時に呼ばれます。
     */
    @Override
    public void onDreamingStopped() {
        super.onDreamingStopped();
        mHandler.removeCallbacks(mRunnbale);
    }

    /**
     * リソースの破棄などに使用します。
     */
    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    /**
     * ジョジョの名言を取得します。
     * 
     * @return
     */
    private String getJojoWords() {

        final String[] words = getResources().getStringArray(R.array.jojo_words);
        final String postWord = ((TextView) mJojoWord.getCurrentView()).getText().toString();

        // 前の文字列と同じでないかチェックする
        int newWordIndex;
        do {
            newWordIndex = MathUtils.randomInt(0, words.length - 1);
        } while (words[newWordIndex].equals(postWord));

        return words[newWordIndex];

    }
}
