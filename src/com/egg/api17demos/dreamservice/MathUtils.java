package com.egg.api17demos.dreamservice;

/**
 * 演算のユーティリティクラスです。
 * 
 * @author sogaroid
 * 
 */
public class MathUtils {

    /**
     * 指定された範囲のランダムの整数値を返します。
     * 
     * @param minValue
     *            最小値
     * @param maxValue
     *            最大値
     * @return minValueからmaxValueの範囲のランダムな整数値を返します。
     */
    public static int randomInt(final int min, final int max) {
        // 最小・最大の逆転を考慮
        final int maxValue = Math.max(min, max);
        final int minValue = Math.min(min, max);
        return (int) (Math.floor(Math.random() * (maxValue - minValue + 1)) + minValue);
    }

}
