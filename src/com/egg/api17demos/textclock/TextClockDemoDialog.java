package com.egg.api17demos.textclock;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextClock;

import com.egg.api17demos.R;

public class TextClockDemoDialog extends DialogFragment {

    /**
     * 時計の表示タイプ
     */
    enum ClockType {
        /** 年月日 */
        TYPE1,
        /** 時間・分・秒 */
        TYPE2,
        /** ミリ秒 */
        TYPE3,
        /** 月・日・曜日 */
        TYPE4,
        /** カラフル */
        TYPE5
    }

    /** ダイアログのタイプのキー */
    static final String KEY_TYPE = "type";

    public static DialogFragment getInstance(final ClockType clockType) {
        final DialogFragment dialog = new TextClockDemoDialog();
        final Bundle bundle = new Bundle();
        bundle.putInt(KEY_TYPE, clockType.ordinal());
        dialog.setArguments(bundle);
        return dialog;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        final Activity activity = getActivity();

        CharSequence format = null;
        switch (ClockType.values()[getArguments().getInt(KEY_TYPE)]) {
        case TYPE1:
            format = activity.getString(R.string.textclock_format_1);
            break;
        case TYPE2:
            format = activity.getString(R.string.textclock_format_2);
            break;
        case TYPE3:
            format = activity.getString(R.string.textclock_format_3);
            break;
        case TYPE4:
            format = activity.getString(R.string.textclock_format_4);
            break;
        case TYPE5:
            format = Html.fromHtml(activity.getString(R.string.textclock_format_5));
            return new AlertDialog.Builder(activity).setView(activity.getLayoutInflater().inflate(R.layout.textclock_colorful, null)).create();
        }

        final TextClock textClock = (TextClock) activity.getLayoutInflater().inflate(R.layout.textclock, null);
        textClock.setFormat24Hour(format);
        return new AlertDialog.Builder(activity).setView(textClock).create();
    }
}
