package com.egg.api17demos.textclock;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.egg.api17demos.R;
import com.egg.api17demos.textclock.TextClockDemoDialog.ClockType;

/**
 * TextClockのデモ画面です。
 * 
 * @author yoshihide-sogawa
 */
public class TextClockDemoActivity extends Activity {

    static final String TAG_TEXTCLOCK_DIALOG = "textclock_dialog";

    /**
     * {@inherited}
     */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_textclock_demo);

        findViewById(R.id.button1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextClockDemoDialog.getInstance(ClockType.TYPE1).show(getFragmentManager(), TAG_TEXTCLOCK_DIALOG);
            }
        });

        findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextClockDemoDialog.getInstance(ClockType.TYPE2).show(getFragmentManager(), TAG_TEXTCLOCK_DIALOG);
            }
        });

        findViewById(R.id.button3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextClockDemoDialog.getInstance(ClockType.TYPE3).show(getFragmentManager(), TAG_TEXTCLOCK_DIALOG);
            }
        });

        findViewById(R.id.button4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextClockDemoDialog.getInstance(ClockType.TYPE4).show(getFragmentManager(), TAG_TEXTCLOCK_DIALOG);
            }
        });

        findViewById(R.id.button5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextClockDemoDialog.getInstance(ClockType.TYPE5).show(getFragmentManager(), TAG_TEXTCLOCK_DIALOG);
            }
        });
    }
}
